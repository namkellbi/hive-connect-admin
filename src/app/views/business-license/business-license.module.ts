import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BusinessLicenseRoutingModule } from './business-license-routing.module';
import { BusinessLicenseComponent } from './business-license.component';
import { SharedLibraryModule } from 'src/shared/shared-library.module';


@NgModule({
  declarations: [
    BusinessLicenseComponent
  ],
  imports: [
    CommonModule,
    BusinessLicenseRoutingModule,
    SharedLibraryModule
  ]
})
export class BusinessLicenseModule { }
