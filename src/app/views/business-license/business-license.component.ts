import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DROPDOWN_JOB_STATUS, MAP_JOB_STATE_VALUE } from '@constant/form';
import { ILicense, JobApplyState } from '@interface/job';
import { IRequestPagination } from '@interface/request';
import { ResponseStatus } from '@interface/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { DEFAULT_PAGINATION } from '@util/defaultObject';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { combineLatest, filter, finalize, map, Observable, startWith, Subject, switchMap, takeUntil, tap } from 'rxjs';

@Component({
  selector: 'app-business-license',
  templateUrl: './business-license.component.html',
  styleUrls: ['./business-license.component.scss']
})
export class BusinessLicenseComponent implements OnInit {
  listReport$ = new Observable<ILicense[]>();
  loading = false;
  loadingStatus = false;
  pagination$ = new Subject<IRequestPagination>();
  total = 0;
  filter$ = new Subject<any>();
  destroy$ = new Subject();
  form!: FormGroup;
  readonly DROPDOWN_JOB_STATUS = DROPDOWN_JOB_STATUS;
  constructor(
    private apiService: ApiService,
    private helperService: HelperService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.listReport$ = combineLatest([
      this.pagination$.asObservable().pipe(startWith(DEFAULT_PAGINATION)),
      this.filter$.pipe(startWith({}))
    ])
      .pipe(
        takeUntil(this.destroy$),
        switchMap(([pagination, form]) => {
          this.loading = true;
          return this.apiService.getLicense({ ...pagination, ...form })
            .pipe(
              finalize(() => this.loading = false),
            )
        }),
        filter(({ status }) => status === ResponseStatus.success),
        // tap(({ data: { pagination } }) => this.total = pagination?.totalRecords),
        map(({ data }) => data)
      )
  }

  initForm() {
    this.form = this.fb.group({
      businessApprovalStatus: JobApplyState.pending,
      additionalApprovalStatus: JobApplyState.pending
    });
  }

  get defaultPagination() {
    return DEFAULT_PAGINATION;
  }

  submit() {
    this.filter$.next(this.form.value);
  }

  onPageChange($event: PageChangedEvent) {
    this.pagination$.next({
      ...DEFAULT_PAGINATION,
      pageNo: $event.page
    })
  }
  approveLicense(license: ILicense, approvalStatus: JobApplyState, type: 1 | 2) {
    const request = {
      "userId": license.requestUserId,
      type,
      approvalStatus
    }
    this.loadingStatus = true;
    this.apiService.changeLicenseStatus(request)
      .pipe(
        finalize(() => this.loadingStatus = false)
      )
      .subscribe({
        next: ({ status, data, message }) => {
          if (status === ResponseStatus.success) {
            this.helperService.showSuccess('', 'Đổi trạng thái thành công');
            if (type === 1) {
              license.businessLicenseApprovalStatus = approvalStatus;
            } else {
              license.additionalLicenseApprovalStatus = approvalStatus;
            }
          } else {
            this.helperService.showError('', message || 'Đổi trạng thái thất bại')
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  get status() {
    return JobApplyState;
  }

  get map() {
    return MAP_JOB_STATE_VALUE
  }
}
