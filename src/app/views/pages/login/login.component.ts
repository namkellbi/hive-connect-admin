import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ResponseStatus } from '@interface/response';
import { ROLE } from '@interface/user';
import { ApiService } from '@services/api.service';
import { AuthService } from '@services/auth.service';
import { HelperService } from '@services/helper.service';
import { finalize } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form!: FormGroup;
  loading = false;
  constructor(
    private apiService: ApiService,
    private helperService: HelperService,
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.form = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  submit() {
    this.helperService.validateFormField(this.form);
    if (this.form.invalid) {
      return;
    }
    const params = this.form.value;
    this.loading = true;
    this.apiService.login(params)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: (response) => {
          if (response.status === ResponseStatus.success) {
            if ((response.data.user.roleId === ROLE.admin)) {
              response.data.user.fullName = response.data.admin.fullName;
              this.authService.loginSuccess(response);
              this.router.navigate(['/dashboard'])
              return;
            }
            this.helperService.showError('', 'Bạn không có quyền');
            return;
          }
          this.helperService.showError('', response.message || 'Đăng nhập thất bại');

        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }
}
