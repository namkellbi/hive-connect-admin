import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedLibraryModule } from 'src/shared/shared-library.module';
import { AccountRoutingModule } from './account-routing.module';
import { AccountComponent } from './account.component';
import { ListReportComponent } from './list-report/list-report.component';
import { ListUserComponent } from './list-user/list-user.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { AccountUserDetailComponent } from './account-user-detail/account-user-detail.component';


@NgModule({
  declarations: [
    AccountComponent,
    ListUserComponent,
    ListReportComponent,
    CreateUserComponent,
    AccountUserDetailComponent
  ],
  imports: [
    CommonModule,
    AccountRoutingModule,
    SharedLibraryModule
  ]
})
export class AccountModule { }
