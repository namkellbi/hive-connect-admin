import { Component, OnInit } from '@angular/core';
import { ConfirmModalComponent } from '@docs-components/confirm-modal/confirm-modal.component';
import { ResponseStatus } from '@interface/response';
import { ROLE } from '@interface/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { finalize, tap } from 'rxjs';
import { DEFAULT_PAGINATION } from '../../../../core/utils/defaultObject';
import { AccountUserDetailComponent } from '../account-user-detail/account-user-detail.component';
import { CreateUserComponent } from '../create-user/create-user.component';
enum Tab {
  Recruiter = 'Recruiter',
  Candidate = 'Candidate',
  Admin = 'Admin'
}

type InfoUnit = 'Today' | 'Month' | 'Total';
@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent implements OnInit {
  request = {
    ...DEFAULT_PAGINATION,
    username: '',
    email: '',
    fullName: '',
    userId: '',
    tab: Tab.Recruiter,
    isLocked: false
  }
  listUser: any[] = [];
  infoAccounts = {
    [ROLE.admin]: { Month: 0, Today: 0, Total: 0 },
    [ROLE.candidate]: { Month: 0, Today: 0, Total: 0 },
    [ROLE.recruiter]: { Month: 0, Today: 0, Total: 0 },
    [ROLE.counselors]: { Month: 0, Today: 0, Total: 0 },
  }
  loading = false;
  totalItems = 0;
  constructor(
    private apiService: ApiService,
    private helperService: HelperService,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
    this.getUser();
  }

  get tabValue() {
    return Tab;
  }

  get role() {
    return ROLE;
  }

  get roleMapper() {
    return {
      [ROLE.admin]: 'Quản trị viên',
      [ROLE.candidate]: 'Ứng Viên',
      [ROLE.recruiter]: 'Nhà tuyển dụng',
      [ROLE.counselors]: 'Tư vấn viên',
    } as any
  }

  changeTab(value: Tab) {
    this.request = {
      ...this.request,
      ...DEFAULT_PAGINATION,
      tab: value
    }
    this.getUser();
  }

  search() {
    this.request.pageNo = 1;
    this.getUser();
  }

  getUser() {
    this.loading = true;
    if (!this.request.userId) { this.request.userId = '0'; }
    this.apiService.searchUser(this.request)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ data, status }) => {
          if (status === ResponseStatus.success) {
            this.listUser = data.data || [];
            this.totalItems = data.pagination.totalRecords
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }
  onPageChange($event: PageChangedEvent) {
    this.request.pageNo = $event.page;
    this.getUser();
  }

  confirmAction() {
    const modal = this.modalService.show(ConfirmModalComponent, {
      backdrop: true,
    });
    return modal.content?.onClose.pipe(
      tap(() => modal.hide())
    );
  }

  activeDeActive(user: any) {
    this.confirmAction()?.subscribe(action => {
      if (action) {
        this.apiService.activeDeActive(user.userId).subscribe({
          next: ({ status, data }) => {
            if (status === ResponseStatus.success) {
              user.isActive = !user.isActive;
            }
          }
        })
      }
    })
  }
  lockUnlock(user: any) {
    this.confirmAction()?.subscribe(action => {
      if (action) {
        this.apiService.lockUnlock(user.userId).subscribe({
          next: ({ status, data }) => {
            if (status === ResponseStatus.success) {
              user.isLocked = !user.isLocked;
            }
          }
        })
      }
    })
  }
  createAccount() {
    const modal = this.modalService.show(CreateUserComponent);
    modal.content?.onClose.subscribe((data) => {
      modal.hide();
      if (data) {
        this.search();
      }
    })
  }

  openDetailAccount(userId: number) {
    const modal = this.modalService.show(AccountUserDetailComponent, {
      class: 'modal-xl',
      initialState: {
        userId
      }
    });
    modal.content?.onClose.subscribe(() => modal.hide());
  }
}
