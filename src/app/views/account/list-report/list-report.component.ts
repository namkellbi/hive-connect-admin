import { Component, OnInit } from '@angular/core';
import { ResponseStatus } from '@interface/response';
import { ROLE } from '@interface/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { DEFAULT_PAGINATION } from '@util/defaultObject';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { finalize } from 'rxjs';
@Component({
  selector: 'app-list-report',
  templateUrl: './list-report.component.html',
  styleUrls: ['./list-report.component.scss']
})
export class ListReportComponent implements OnInit {
  request = {
    ...DEFAULT_PAGINATION,
    username: '',
    personReportName: '',
  }
  listUser: any[] = [];
  loading = false;
  totalItems = 0;
  constructor(
    private apiService: ApiService,
    private helperService: HelperService
  ) { }

  ngOnInit(): void {
    this.getUser();
  }

  search() {
    this.request.pageNo = 1;
    this.getUser();
  }

  getUser() {
    this.loading = true;
    this.apiService.searchReportUser(this.request)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ data, status }) => {
          if (status === ResponseStatus.success) {
            this.listUser = data.data?.content || [];
            this.totalItems = data.data.totalElements
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }
  onPageChange($event: PageChangedEvent) {
    this.request.pageNo = $event.page;
    this.getUser();
  }

  get mapRole() {
    return {
      [ROLE.recruiter]: 'Nhà tuyển dụng',
      [ROLE.candidate]: 'Người tìm việc'
    }
  }
}
