import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountUserDetailComponent } from './account-user-detail.component';

describe('AccountUserDetailComponent', () => {
  let component: AccountUserDetailComponent;
  let fixture: ComponentFixture<AccountUserDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountUserDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountUserDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
