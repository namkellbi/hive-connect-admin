import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ICandidateCV, ICandidateProfile, IRecruiterProfile, IUser, ROLE } from '@interface/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { finalize, switchMap } from 'rxjs';

@Component({
  selector: 'app-account-user-detail',
  templateUrl: './account-user-detail.component.html',
  styleUrls: ['./account-user-detail.component.scss']
})
export class AccountUserDetailComponent implements OnInit {
  @Output() onClose = new EventEmitter();
  loading = true;
  userId = 0;
  recruiter = {} as IRecruiterProfile;
  candidate = {} as ICandidateProfile;
  info = {} as IUser;
  cv = {} as ICandidateCV;
  constructor(
    private apiService: ApiService,
    private helperService: HelperService
  ) { }

  ngOnInit(): void {
    this.apiService.getUserInfo(this.userId)
      .pipe(
        switchMap(({ data }) => {
          this.info = data;
          return data.roleId === ROLE.candidate
            ? this.apiService.getCandidateProfile(this.userId)
            : this.apiService.getRecruiterProfile(this.userId)
        }),
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ data }) => {
          if (this.info.roleId === ROLE.candidate) {
            this.candidate = data as ICandidateProfile;
            this.getCv();
          }
          if (this.info.roleId === ROLE.recruiter) {
            this.recruiter = data as IRecruiterProfile;
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  getCv() {
    this.apiService.getCandidateCV(this.candidate.id).subscribe({
      next: ({ data }) => {
        this.cv = data;
      },
      error: error => this.helperService.callApiFailedHandler(error)
    })
  }
}
