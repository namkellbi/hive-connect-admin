import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ResponseStatus } from '@interface/response';
import { ROLE } from '@interface/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { finalize } from 'rxjs';
@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  @Output() onClose = new EventEmitter();
  form!: FormGroup;
  loading = false;
  constructor(
    private fb: FormBuilder,
    private helperService: HelperService,
    private apiService: ApiService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email, Validators.pattern(/^[a-zA-Z0-9.! #$%&'*+\=? ^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]],
      // username: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.pattern(/^(?=.*[a-z]){1}(?=.*[A-Z]){1}(?=.*[0-9]){1}(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]){1}.{4,50}$/)]],
      confirmPassword: ['', [Validators.required]],
      fullName: [],
      roleId: [ROLE.admin]
    });
  }

  close(data?: any) {
    this.onClose.emit(data);
  }

  submit() {
    this.helperService.validateFormField(this.form);
    if (this.form.invalid) {
      return;
    }
    const params = this.form.value;
    this.loading = true;
    this.apiService.register(params)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ status, data, message }) => {
          if (status === ResponseStatus.success) {
            this.helperService.showSuccess('', 'Tạo tài khoản thành công');
            this.close(data);
          } else {
            this.helperService.showError('', message || 'Tạo tài khoản thất bại');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  get role() {
    return ROLE;
  }
}
