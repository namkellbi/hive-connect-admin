import { Component, OnInit } from '@angular/core';
import { ResponseStatus } from '@interface/response';
import { ROLE } from '@interface/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';

type InfoUnit = 'Today' | 'Month' | 'Total';
interface IUser {
  name: string;
  state: string;
  registered: string;
  country: string;
  usage: number;
  period: string;
  payment: string;
  activity: string;
  avatar: string;
  status: string;
  color: string;
}

@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  infoAccounts = {
    [ROLE.admin]: { Month: 0, Today: 0, Total: 0 },
    [ROLE.candidate]: { Month: 0, Today: 0, Total: 0 },
    [ROLE.recruiter]: { Month: 0, Today: 0, Total: 0 },
    [ROLE.counselors]: { Month: 0, Today: 0, Total: 0 },
  };
  constructor(
    private apiService: ApiService,
    private helperService: HelperService
  ) {
  }


  ngOnInit(): void {
    this.getAccountsInfo();
  }
  getAccountsInfo() {
    this.apiService.getAccountInfo().subscribe({
      next: ({ status, data }) => {
        if (status === ResponseStatus.success) {
          Object.entries(data).forEach(([key, value]) => {
            (value as any).forEach(({ roleId, numberUser }: { roleId: ROLE, numberUser: number }) => {
              this.infoAccounts[roleId][(key as unknown as InfoUnit)] = numberUser;
            })
          })
        }
      }
    })
  }
  get role() {
    return ROLE;
  }
}
