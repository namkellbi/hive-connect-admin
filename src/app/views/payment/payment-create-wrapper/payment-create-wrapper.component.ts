import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PaymentRental } from '@interface/payment';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-payment-create-wrapper',
  templateUrl: './payment-create-wrapper.component.html',
  styleUrls: ['./payment-create-wrapper.component.scss']
})
export class PaymentCreateWrapperComponent implements OnInit {
  destroy$ = new Subject();
  rentalPackageId = PaymentRental.candidate;
  constructor(
    private activatedRoute: ActivatedRoute,
    private _location: Location
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(params => {
        this.rentalPackageId = params['rentalId'];
      })
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  get paymentRental() {
    return PaymentRental;
  }

  onClose() {
    this._location.back();
  }
}
