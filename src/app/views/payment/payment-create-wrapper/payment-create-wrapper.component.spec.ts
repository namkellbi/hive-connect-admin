import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentCreateWrapperComponent } from './payment-create-wrapper.component';

describe('PaymentCreateWrapperComponent', () => {
  let component: PaymentCreateWrapperComponent;
  let fixture: ComponentFixture<PaymentCreateWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentCreateWrapperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentCreateWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
