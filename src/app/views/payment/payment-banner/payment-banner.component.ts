import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PaymentRental } from '@interface/payment';
import { IResponse } from '@interface/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';
import { PaymentBaseComponent } from '../payment-base/payment-base.component';
import { PaymentCreatePackageBannerComponent } from '../payment-create-package-banner/payment-create-package-banner.component';
@Component({
  selector: 'app-payment-banner',
  templateUrl: './payment-banner.component.html',
  styleUrls: ['./payment-banner.component.scss']
})
export class PaymentBannerComponent extends PaymentBaseComponent implements OnInit {

  constructor(
    apiService: ApiService,
    modalService: BsModalService,
    helperService: HelperService,
    router: Router
  ) {
    super(apiService, modalService, helperService, router);
    this.filter.rentalPackageId = PaymentRental.banner;
  }

  override getApi(): Observable<IResponse<any>> {
    return this.apiService.getListBanner(this.filter);
  }

  override getModalComponent() {
    return PaymentCreatePackageBannerComponent;
  }
}
