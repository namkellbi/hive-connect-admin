import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentCreatePackageComponent } from './payment-create-package.component';

describe('PaymentCreatePackageComponent', () => {
  let component: PaymentCreatePackageComponent;
  let fixture: ComponentFixture<PaymentCreatePackageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentCreatePackageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentCreatePackageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
