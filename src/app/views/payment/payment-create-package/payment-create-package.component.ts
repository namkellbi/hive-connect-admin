import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MAP_RENTAL_PACKAGE } from '@constant/form';
import { PaymentRental } from '@interface/payment';
import { ResponseStatus } from '@interface/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { noWhiteSpace } from '@util/form';
import { filter, finalize, Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-payment-create-package',
  templateUrl: './payment-create-package.component.html',
  styleUrls: ['./payment-create-package.component.scss']
})
export class PaymentCreatePackageComponent implements OnInit {
  @Input() rentalPackageId = PaymentRental.candidate;
  @Output() onClose = new EventEmitter();
  form!: FormGroup;
  data: any;
  loading = false;
  destroy$ = new Subject();
  constructor(
    protected apiService: ApiService,
    protected helperService: HelperService,
    protected fb: FormBuilder,
    protected activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(
        takeUntil(this.destroy$),
        filter(params => !!params['id'])
      )
      .subscribe(params => {
        this.initForm();
        this.getDetail(+params['id']);
      });
    this.initForm();
  }

  getDetail(id: number) {
    this.loading = true;
    this.getApiGetDetail(id)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            const { rentalPackageId } = this.activatedRoute.snapshot.queryParams;
            this.data = +rentalPackageId === +PaymentRental.banner ? data.bannerPackage : data.normalPackage;
            this.updateFormValue();
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  getApiGetDetail(id: number) {
    const { rentalPackageId } = this.activatedRoute.snapshot.queryParams;
    return this.apiService.getDetailPackage(id, rentalPackageId);
  }

  initForm() {
    this.configForm();
    this.updateFormValue();
  }

  updateFormValue() {
    if (this.data) {
      const timeExpired = (this.data.timeExpired as string).match(/[0-9]/g);
      this.form.patchValue({
        ...this.data,
        timeExpired: timeExpired?.join('') || ''
      });
    }
  }

  configForm() {
    this.form = this.fb.group({
      rentalPackageId: [this.rentalPackageId],
      detailName: ['', [Validators.required, noWhiteSpace]],
      price: ['', [Validators.required, Validators.min(50000)]],
      discount: ['', []],
      timeExpired: ['', [Validators.required, Validators.min(0)]],
      description: ['', []],
      goldenHour: false,
      normalHour: false,
      maxUploadSixImage: false,
      maxCvView: ['', [Validators.required, Validators.min(0)]],
      deleted: false,
      goodJob: false,
      urgent: false,
      hot: false,
      relatedJob: false,
      suggestJob: false
    });
  }

  submit() {
    this.form.markAllAsTouched();
    console.log(this.form);
    if (this.form.invalid) return;
    const params = this.getFormValue();
    this.loading = true;
    this.getApi(params).pipe(
      finalize(() => this.loading = false)
    )
      .subscribe({
        next: ({ status, data, message }) => {
          if (status === ResponseStatus.success) {
            this.helperService.showSuccess('', `${this.data ? 'Sửa' : 'Tạo'} thành công`);
            this.onClose.emit(data);
          } else {
            this.helperService.showError('', message || `${this.data ? 'Sửa' : 'Tạo'} thất bại`)
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  getFormValue() {
    const params = { ...this.form.value };
    params.timeExpired = `${params.timeExpired} ngày`
    if (this.data) {
      params.id = this.data.id;
    }
    return params;
  }

  getApi(params: any) {
    return this.data ? this.apiService.updatePackage(params) : this.apiService.createPackage(params);
  }


  get rentalName() {
    return MAP_RENTAL_PACKAGE[this.rentalPackageId];
  }

  get paymentRental() {
    return PaymentRental;
  }

  get titleModal() {
    let prefix = 'Tạo';
    if (this.data) {
      prefix = 'Sửa'
    }
    return {
      [PaymentRental.advertisement]: `${prefix} ${MAP_RENTAL_PACKAGE[this.rentalPackageId].toLowerCase()}`,
      [PaymentRental.candidate]: `${prefix} ${MAP_RENTAL_PACKAGE[this.rentalPackageId].toLowerCase()}`,
      [PaymentRental.banner]: `${prefix} gói ${MAP_RENTAL_PACKAGE[this.rentalPackageId].toLowerCase()}`,
    }[this.rentalPackageId];
  }

  get btnLabel() {
    return this.data ? 'Sửa gói dịch vụ' : 'Tạo gói dịch vụ'
  }
}
