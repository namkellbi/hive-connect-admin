import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentCandidateComponent } from './payment-candidate.component';

describe('PaymentCandidateComponent', () => {
  let component: PaymentCandidateComponent;
  let fixture: ComponentFixture<PaymentCandidateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentCandidateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentCandidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
