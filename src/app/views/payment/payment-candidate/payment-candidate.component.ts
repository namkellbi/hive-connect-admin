import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PaymentRental } from '@interface/payment';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { PaymentBaseComponent } from '../payment-base/payment-base.component';

@Component({
  selector: 'app-payment-candidate',
  templateUrl: './payment-candidate.component.html',
  styleUrls: ['./payment-candidate.component.scss']
})
export class PaymentCandidateComponent extends PaymentBaseComponent implements OnInit {

  constructor(
    apiService: ApiService,
    modalService: BsModalService,
    helperService: HelperService,
    router: Router
  ) {
    super(apiService, modalService, helperService, router);
    this.filter.rentalPackageId = PaymentRental.candidate;
  }

}
