import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedLibraryModule } from 'src/shared/shared-library.module';
import { PaymentBannerComponent } from './payment-banner/payment-banner.component';
import { PaymentRoutingModule } from './payment-routing.module';
import { PaymentComponent } from './payment.component';
import { PaymentCandidateComponent } from './payment-candidate/payment-candidate.component';
import { PaymentAdvertisementComponent } from './payment-advertisement/payment-advertisement.component';
import { PaymentCreatePackageComponent } from './payment-create-package/payment-create-package.component';
import { PaymentBaseComponent } from './payment-base/payment-base.component';
import { PaymentCreatePackageBannerComponent } from './payment-create-package-banner/payment-create-package-banner.component';
import { PaymentCreateWrapperComponent } from './payment-create-wrapper/payment-create-wrapper.component';
import { PaymentCreatePackageMainComponent } from './payment-create-package-main/payment-create-package-main.component';

@NgModule({
  declarations: [
    PaymentComponent,
    PaymentBannerComponent,
    PaymentCandidateComponent,
    PaymentAdvertisementComponent,
    PaymentCreatePackageComponent,
    PaymentBaseComponent,
    PaymentCreatePackageBannerComponent,
    PaymentCreateWrapperComponent,
    PaymentCreatePackageMainComponent
  ],
  imports: [
    CommonModule,
    PaymentRoutingModule,
    SharedLibraryModule
  ]
})
export class PaymentModule { }
