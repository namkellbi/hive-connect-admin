import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaymentAdvertisementComponent } from './payment-advertisement/payment-advertisement.component';
import { PaymentBannerComponent } from './payment-banner/payment-banner.component';
import { PaymentCandidateComponent } from './payment-candidate/payment-candidate.component';
import { PaymentCreateWrapperComponent } from './payment-create-wrapper/payment-create-wrapper.component';
import { PaymentComponent } from './payment.component';

const routes: Routes = [{
  path: '', component: PaymentComponent, children: [
    { path: 'candidate', component: PaymentCandidateComponent },
    { path: 'advertisement', component: PaymentAdvertisementComponent },
    { path: 'banner', component: PaymentBannerComponent },
    { path: ':rentalId/create', component: PaymentCreateWrapperComponent },
    { path: ':rentalId/edit/:id', component: PaymentCreateWrapperComponent },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentRoutingModule { }
