import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { noWhiteSpace } from '@util/form';
import { PaymentCreatePackageComponent } from '../payment-create-package/payment-create-package.component';
enum MainPackageRadioField {
  isPopularJob = 'popularJob',
  isNewJob = 'newJob',
  isUrgentJob = 'urgentJob'
}
@Component({
  selector: 'app-payment-create-package-main',
  templateUrl: './payment-create-package-main.component.html',
  styleUrls: ['./payment-create-package-main.component.scss']
})
export class PaymentCreatePackageMainComponent extends PaymentCreatePackageComponent implements OnInit {

  constructor(
    apiService: ApiService,
    helperService: HelperService,
    fb: FormBuilder,
    activatedRoute: ActivatedRoute
  ) {
    super(apiService, helperService, fb, activatedRoute);
  }

  override configForm(): void {
    this.form = this.fb.group({
      rentalPackageId: [this.rentalPackageId],
      detailName: ['', [Validators.required, noWhiteSpace]],
      price: ['', Validators.required],
      discount: [''],
      timeExpired: [''],
      description: [''],
      isRelatedJob: [],
      isSuggestJob: [],
      maxCvView: ['',],
      radioField: ['',]
    });
  }
  override updateFormValue(): void {
    super.updateFormValue();
    if (this.data) {
      let field = '';
      Object.entries(MainPackageRadioField).forEach(([key, value]) => {
        if (this.data[value]) {
          field = value;
        }
      });
      this.form.patchValue({ radioField: field });
    }
  }
  override getFormValue() {
    const formValue = super.getFormValue();
    Object.entries(MainPackageRadioField).forEach(([key, value]) => {
      if (formValue.radioField === value) {
        formValue[value] = true;
      } else {
        formValue[value] = false;
      }
    })
    return formValue;
  }
  get radioFieldValue() {
    return MainPackageRadioField;
  }
  override submit(): void {
    const { isRelatedJob, isSuggestJob, radioField } = this.form.value;
    if (!radioField) {
      this.helperService.showError('', 'Bạn phải chọn hiển chung');
      return;
    }

    super.submit();
  }
}
