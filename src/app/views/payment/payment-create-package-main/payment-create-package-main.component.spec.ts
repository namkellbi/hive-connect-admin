import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentCreatePackageMainComponent } from './payment-create-package-main.component';

describe('PaymentCreatePackageMainComponent', () => {
  let component: PaymentCreatePackageMainComponent;
  let fixture: ComponentFixture<PaymentCreatePackageMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentCreatePackageMainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentCreatePackageMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
