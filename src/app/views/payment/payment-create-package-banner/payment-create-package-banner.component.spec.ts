import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentCreatePackageBannerComponent } from './payment-create-package-banner.component';

describe('PaymentCreatePackageBannerComponent', () => {
  let component: PaymentCreatePackageBannerComponent;
  let fixture: ComponentFixture<PaymentCreatePackageBannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentCreatePackageBannerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentCreatePackageBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
