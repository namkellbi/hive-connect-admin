import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IResponse } from '@interface/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { noWhiteSpace } from '@util/form';
import { Observable } from 'rxjs';
import { PaymentCreatePackageComponent } from '../payment-create-package/payment-create-package.component';

@Component({
  selector: 'app-payment-create-package-banner',
  templateUrl: './payment-create-package-banner.component.html',
  styleUrls: ['./payment-create-package-banner.component.scss']
})
export class PaymentCreatePackageBannerComponent extends PaymentCreatePackageComponent implements OnInit {

  constructor(
    apiService: ApiService,
    helperService: HelperService,
    fb: FormBuilder,
    activatedRoute: ActivatedRoute
  ) {
    super(apiService, helperService, fb, activatedRoute);
  }

  // override getApiGetDetail(id: number): Observable<IResponse<any>> {
  //   return this.apiService.getDetailBannerPackage(id);
  // }

  override configForm(): void {
    this.form = this.fb.group({
      rentalPackageId: this.rentalPackageId,
      price: ['', [Validators.required, Validators.min(50000)]],
      discount: 0,
      timeExpired: ['', [Validators.required, Validators.min(1)]],
      title: ['', [Validators.required, noWhiteSpace]],
      description: [''],
      image: [''],
      spotlight: false,
      homepageBannerA: false,
      homePageBannerB: false,
      homePageBannerC: false,
      jobBannerA: false,
      jobBannerB: false,
      jobBannerC: false
    })
  }

  override updateFormValue(): void {
    super.updateFormValue();
    if (this.data) {
      this.form.patchValue({
        homePageBannerB: this.data.homepageBannerB,
        homePageBannerC: this.data.homepageBannerC,
      })
    }
  }

  override getApi(params: any): Observable<IResponse<any>> {
    return this.data ? this.apiService.updateBannerPackage(params) : this.apiService.createPackageBanner(params);
  }

  override getFormValue() {
    const params = super.getFormValue();
    if (this.data) {
      params.bannerId = this.data.id;
    }
    return params;
  }
  override submit(): void {
    const { rentalPackageId, price, discount, timeExpired, title, description, image, ...position } = this.form.value;
    if (Object.entries(position).every(([_, value]) => !value)) {
      this.helperService.showError('', 'Bạn phải chọn ít nhất 1 vị trí');
      return;
    }
    super.submit();
  }
}
