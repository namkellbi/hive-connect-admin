import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DROPDOWN_RENTAL_PACKAGE } from '@constant/form';
import { PaymentRental } from '@interface/payment';
import { ResponseStatus } from '@interface/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { DEFAULT_PAGINATION } from '@util/defaultObject';
import { BsModalService } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { finalize } from 'rxjs';
import { PaymentCreatePackageComponent } from '../payment-create-package/payment-create-package.component';

@Component({
  selector: 'app-payment-base',
  templateUrl: './payment-base.component.html',
  styleUrls: ['./payment-base.component.scss']
})
export class PaymentBaseComponent implements OnInit {
  listRentalPackage: any[] = [];
  filter = {
    name: '',
    rentalPackageId: PaymentRental.candidate,
    status: '',
    ...DEFAULT_PAGINATION
  }
  package: any[] = [];
  totals = 0;
  loading = false;
  constructor(
    protected apiService: ApiService,
    protected modalService: BsModalService,
    protected helperService: HelperService,
    protected router: Router
  ) { }

  ngOnInit(): void {
    this.getListPackage();
  }

  getApi() {
    return this.apiService.listPackage(this.filter);
  }

  search() {
    this.getListPackage();
  }

  getListPackage() {
    this.loading = true;
    this.getApi()
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.package = data.data;
            this.totals = data.pagination.totalRecords;
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      });
  }

  createPackage(index?: number, data?: any) {
    // const modal = this.modalService.show(this.getModalComponent(), {
    //   initialState: {
    //     data,
    //     rentalPackageId: this.filter.rentalPackageId
    //   }
    // }
    // );
    // modal.content?.onClose.subscribe(result => {
    //   if (result) {
    //     this.getListPackage();
    //   }
    //   modal.hide();
    // })
    if (data) {
      this.router.navigate(['/payment', this.filter.rentalPackageId, 'edit', data.id], {
        queryParams: {
          rentalPackageId: this.filter.rentalPackageId
        }
      });
      return;
    }
    this.router.navigate(['/payment', this.filter.rentalPackageId, 'create'], {
      queryParams: {
        rentalPackageId: this.filter.rentalPackageId
      }
    });
  }

  getModalComponent() {
    return PaymentCreatePackageComponent;
  }

  pageChange($event: PageChangedEvent) {
    this.filter.pageNo = $event.page;
    this.getListPackage();
  }

  get btnCreateLabel() {
    return {
      [PaymentRental.banner]: 'Tạo gói banner',
      [PaymentRental.advertisement]: 'Tạo gói dịch vụ chính',
      [PaymentRental.candidate]: 'Tạo gói hồ sơ ứng viên',

    }[this.filter.rentalPackageId]
  }
}
