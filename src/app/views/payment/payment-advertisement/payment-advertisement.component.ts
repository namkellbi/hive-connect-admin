import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PaymentRental } from '@interface/payment';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { PaymentBaseComponent } from '../payment-base/payment-base.component';

@Component({
  selector: 'app-payment-advertisement',
  templateUrl: './payment-advertisement.component.html',
  styleUrls: ['./payment-advertisement.component.scss']
})
export class PaymentAdvertisementComponent extends PaymentBaseComponent implements OnInit {

  constructor(
    apiService: ApiService,
    modalService: BsModalService,
    helperService: HelperService,
    router: Router
  ) {
    super(apiService, modalService, helperService, router);
    this.filter.rentalPackageId = PaymentRental.advertisement;
  }

}
