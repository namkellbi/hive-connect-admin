import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentAdvertisementComponent } from './payment-advertisement.component';

describe('PaymentAdvertisementComponent', () => {
  let component: PaymentAdvertisementComponent;
  let fixture: ComponentFixture<PaymentAdvertisementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentAdvertisementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentAdvertisementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
