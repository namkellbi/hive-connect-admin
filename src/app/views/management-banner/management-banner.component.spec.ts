import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementBannerComponent } from './management-banner.component';

describe('ManagementBannerComponent', () => {
  let component: ManagementBannerComponent;
  let fixture: ComponentFixture<ManagementBannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementBannerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
