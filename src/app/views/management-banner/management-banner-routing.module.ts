import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManagementBannerComponent } from './management-banner.component';

const routes: Routes = [{ path: '', component: ManagementBannerComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagementBannerRoutingModule { }
