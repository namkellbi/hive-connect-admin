import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManagementBannerRoutingModule } from './management-banner-routing.module';
import { ManagementBannerComponent } from './management-banner.component';
import { SharedLibraryModule } from 'src/shared/shared-library.module';


@NgModule({
  declarations: [
    ManagementBannerComponent
  ],
  imports: [
    CommonModule,
    ManagementBannerRoutingModule,
    SharedLibraryModule
  ]
})
export class ManagementBannerModule { }
