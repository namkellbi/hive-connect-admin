import { Component, OnInit } from '@angular/core';
import { MAP_JOB_STATE_VALUE } from '@constant/form';
import { ConfirmModalComponent } from '@docs-components/confirm-modal/confirm-modal.component';
import { JobApplyState } from '@interface/job';
import { BannerPosition, IBannerUserRequest } from '@interface/payment';
import { IRequestPagination } from '@interface/request';
import { ResponseStatus } from '@interface/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { DEFAULT_PAGINATION } from '@util/defaultObject';
import moment from 'moment';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { enGbLocale } from 'ngx-bootstrap/locale';
import { BsModalService } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { BehaviorSubject, catchError, finalize, map, Observable, of, Subject, switchMap, takeUntil, tap } from 'rxjs';
@Component({
  selector: 'app-management-banner',
  templateUrl: './management-banner.component.html',
  styleUrls: ['./management-banner.component.scss']
})
export class ManagementBannerComponent implements OnInit {
  list$ = new Observable<IBannerUserRequest[]>();
  pagination$ = new BehaviorSubject<IRequestPagination>({ ...DEFAULT_PAGINATION });
  filter = {
    screenName: '',
    from: moment().subtract(1, 'month').toDate(),
    to: moment().toDate()
  };
  loading = false;
  totals = 0;
  destroy$ = new Subject();
  selectedItem: any;

  constructor(
    private apiService: ApiService,
    private helperService: HelperService,
    private modalService: BsModalService,
    private localeService: BsLocaleService
  ) {
    enGbLocale.invalidDate = 'Ngày không hợp lệ';
    defineLocale('custom locale', enGbLocale);
    this.localeService.use('custom locale');
  }

  ngOnInit(): void {
    this.getRequest();
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  getRequest() {
    this.list$ = this.pagination$
      .pipe(
        takeUntil(this.destroy$),
        switchMap(pagination => {
          this.loading = true;
          return this.apiService.getBannerRequest({
            ...pagination,
            ...this.filter,
            from: moment(this.filter.from).format('yyyy-MM-DD'),
            to: moment(this.filter.to).format('yyyy-MM-DD')
          }).pipe(
            finalize(() => this.loading = false)
          )
        }),
        map(({ status, data, message }) => {
          if (status === ResponseStatus.success) {
            this.totals = data.pagination.totalRecords;
            return data.data;
          }
          throw new Error(message);
        }),
        catchError((error) => {
          this.helperService.callApiFailedHandler(error);
          return of([]);
        })
      )
  }
  confirmAction() {
    const modal = this.modalService.show(ConfirmModalComponent, {
      backdrop: true,
    });
    return modal.content?.onClose.pipe(
      tap(() => modal.hide())
    );
  }
  updateStatus(approvalStatus: JobApplyState, item: any) {
    this.confirmAction()?.subscribe(data => {
      if (data) {
        const request = {
          approvalStatus,
          bannerActiveId: item.bannerActiveId
        }
        this.loading = true;
        this.apiService.updateBannerRequest(request)
          .pipe(
            finalize(() => this.loading = false)
          )
          .subscribe({
            next: ({ status, data, message }) => {
              if (status === ResponseStatus.success) {
                this.helperService.showSuccess('', 'Cập nhật trạng thái thành công');
                item.approvalStatus = approvalStatus;
              } else {
                this.helperService.showError('', message || 'Cập nhật thất bại');
              }
            },
            error: error => this.helperService.callApiFailedHandler(error)
          })
      }
    })

  }
  pageChange($event: PageChangedEvent) {
    this.pagination$.next({
      ...this.pagination$.getValue(),
      pageNo: $event.page
    });
  }

  search() {
    this.pagination$.next({ ...DEFAULT_PAGINATION });
  }

  preview(temp: any, item: any) {
    this.selectedItem = item;
    this.modalService.show(temp);
  }
  get status() {
    return JobApplyState;
  }
  get pagination() {
    return DEFAULT_PAGINATION;
  }
  get map() {
    return MAP_JOB_STATE_VALUE;
  }
  get bannerTitle() {
    return {
      [BannerPosition.homepageBannerA]: 'Trang chủ khu vực A',
      [BannerPosition.homepageBannerB]: 'Trang chủ khu vực B',
      [BannerPosition.homepageBannerC]: 'Trang chủ khu vực C',
      [BannerPosition.jobBannerA]: 'Trang công việc khu vực A',
      [BannerPosition.jobBannerB]: 'Trang công việc khu vực B',
      [BannerPosition.jobBannerC]: 'Trang công việc khu vực C',
      [BannerPosition.spotlight]: 'Popup khi vào trang chủ',
    } as { [key: string]: string };
  }
}
