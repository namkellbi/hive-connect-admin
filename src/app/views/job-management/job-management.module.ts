import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JobManagementRoutingModule } from './job-management-routing.module';
import { JobManagementComponent } from './job-management.component';
import { JobListComponent } from './job-list/job-list.component';
import { JobReportComponent } from './job-report/job-report.component';
import { SharedLibraryModule } from 'src/shared/shared-library.module';
import { ReportJobComponent } from './report-job/report-job.component';

@NgModule({
  declarations: [
    JobManagementComponent,
    JobListComponent,
    JobReportComponent,
    ReportJobComponent
  ],
  imports: [
    CommonModule,
    JobManagementRoutingModule,
    SharedLibraryModule
  ]
})
export class JobManagementModule { }
