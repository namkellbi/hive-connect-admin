import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { JobDetail } from 'src/core/interfaces/job';

@Component({
  selector: 'hc-report-job',
  templateUrl: './report-job.component.html',
  styleUrls: ['./report-job.component.scss']
})
export class ReportJobComponent implements OnInit {
  @Input() job = {} as JobDetail;
  @Output() onClose = new EventEmitter();
  form!: FormGroup;
  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      reason: ['', [Validators.required]],
      fullName: ['', [Validators.required]],
      phone: ['',],
      address: ['',],
      email: ['', [Validators.required, Validators.email]]
    });
  }

}
