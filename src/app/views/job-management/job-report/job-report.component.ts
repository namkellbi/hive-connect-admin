import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAP_JOB_STATE_VALUE } from '@constant/form';
import { ConfirmModalComponent } from '@docs-components/confirm-modal/confirm-modal.component';
import { JobApplyState, ReportJob } from '@interface/job';
import { IRequestPagination } from '@interface/request';
import { ResponseStatus } from '@interface/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { defaultPaginationRequest, defaultRequest, DEFAULT_PAGINATION } from '@util/defaultObject';
import { BsModalService } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { BehaviorSubject, catchError, combineLatest, filter, finalize, map, Observable, startWith, Subject, switchMap, takeUntil, tap } from 'rxjs';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { enGbLocale } from 'ngx-bootstrap/locale';
import moment from 'moment';
@Component({
  selector: 'app-job-report',
  templateUrl: './job-report.component.html',
  styleUrls: ['./job-report.component.scss']
})
export class JobReportComponent implements OnInit, OnDestroy {
  listReport$ = new Observable<ReportJob[]>();
  loading = false;
  pagination$ = new Subject<IRequestPagination>();
  total = 0;
  filter$ = new BehaviorSubject<any>({});
  destroy$ = new Subject();
  form!: FormGroup;
  constructor(
    private apiService: ApiService,
    private helperService: HelperService,
    private fb: FormBuilder,
    private modalService: BsModalService,
    private localeService: BsLocaleService
  ) {
    enGbLocale.invalidDate = 'Ngày không hợp lệ';
    defineLocale('custom locale', enGbLocale);
    this.localeService.use('custom locale');
  }

  ngOnInit(): void {
    this.initForm();
    this.listReport$ = combineLatest([
      this.pagination$.asObservable().pipe(startWith(DEFAULT_PAGINATION)),
      this.filter$
    ])
      .pipe(
        takeUntil(this.destroy$),
        switchMap(([pagination, form]) => {
          this.loading = true;
          return this.apiService.getReportJobs({
            ...pagination,
            ...form,
            createdAtFrom: this.format(form.createdAtFrom),
            createdAtTo: this.format(form.createdAtTo),
            updatedAtFrom: this.format(form.updatedAtFrom),
            updatedAtTo: this.format(form.updatedAtToks)
          })

            .pipe(
              finalize(() => this.loading = false),
              catchError(error => defaultPaginationRequest<ReportJob>())
            )
        }),
        filter(({ status }) => status === ResponseStatus.success),
        tap(({ data: { pagination } }) => this.total = pagination.totalRecords),
        map(({ data: { data, pagination } }) => data)
      )

  }

  format(data: string) {
    return moment(data).format('yyyy-MM-DD');
  }

  initForm() {
    this.form = this.fb.group({
      createdAtFrom: [moment().subtract(1, 'month').toDate()],
      createdAtTo: [moment().toDate()],
      updatedAtFrom: [''],
      updatedAtTo: [''],
      jobName: ['']
    });
  }

  submit() {
    const { createdAtFrom, createdAtTo, updatedAtFrom, updatedAtTo } = this.form.value;
    this.filter$.next({
      ...this.form.value,
      createdAtFrom: createdAtFrom ? createdAtFrom?.toISOString() : '',
      createdAtTo: createdAtTo ? createdAtTo?.toISOString() : '',
      updatedAtFrom: updatedAtFrom ? updatedAtFrom?.toISOString() : '',
      updatedAtTo: updatedAtTo ? updatedAtTo?.toISOString() : '',

    });
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  get defaultPagination() {
    return DEFAULT_PAGINATION;
  }

  get map() {
    return MAP_JOB_STATE_VALUE;
  }

  get status() {
    return JobApplyState;
  }

  getListReport() {

  }

  onPageChange($event: PageChangedEvent) {
    this.pagination$.next({
      ...DEFAULT_PAGINATION,
      pageNo: $event.page
    })
  }
  confirmAction(approvalStatus: JobApplyState) {
    const modal = this.modalService.show(ConfirmModalComponent, {
      backdrop: true,
      initialState: {
        message: approvalStatus === JobApplyState.approve ? `Bạn có chắc chấp thuận không? Khi thực hiện thì bài đăng công việc này sẽ bị ẩn đi` : `Bạn có chắc từ chối không?`
      }
    });
    return modal.content?.onClose.pipe(
      tap(() => modal.hide())
    );
  }
  deleteJob(item: any, approvalStatus: JobApplyState) {
    this.confirmAction(approvalStatus)?.subscribe(data => {
      if (data) {
        this.loading = true;
        const request = {
          reportId: item.reportId,
          approvalStatus
        }
        this.apiService.updateStatusReport(request)
          .pipe(
            finalize(() => this.loading = true)
          )
          .subscribe({
            next: ({ status, data, message }) => {
              if (status === ResponseStatus.success) {
                this.helperService.showSuccess('', 'Cập nhật thành công');
                this.submit();
              } else {
                this.helperService.showError('', message || 'Cập nhật thất bại');
              }
            },
            error: error => this.helperService.callApiFailedHandler(error)
          })
      }
    });
  }
}
