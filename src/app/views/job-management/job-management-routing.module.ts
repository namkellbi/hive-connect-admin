import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JobListComponent } from './job-list/job-list.component';
import { JobManagementComponent } from './job-management.component';
import { JobReportComponent } from './job-report/job-report.component';

const routes: Routes = [{
  path: '', component: JobManagementComponent, children: [
    { path: 'list', component: JobListComponent },
    { path: 'report', component: JobReportComponent }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobManagementRoutingModule { }
