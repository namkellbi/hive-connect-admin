import { CurrencyPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DROPDOWN_JOB_STYLE, MAP_JOB_STYLE_NAME } from '@constant/form';
import { ConfirmModalComponent } from '@docs-components/confirm-modal/confirm-modal.component';
import { environment } from '@environment/environment.prod';
import { IDropDownItem } from '@interface/form';
import { Job, JobApplyState, JobDetail } from '@interface/job';
import { ResponseStatus } from '@interface/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { DEFAULT_PAGINATION, DEFAULT_SEARCH } from '@util/defaultObject';
import { BsModalService } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { finalize, tap } from 'rxjs';
import { ReportJobComponent } from '../report-job/report-job.component';

@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.scss']
})
export class JobListComponent implements OnInit {
  pagination = { ...DEFAULT_PAGINATION };
  form!: FormGroup;
  loading = false;
  jobs: JobDetail[] = [];
  totalPage = 0;
  domain = environment.USER_DOMAIN;
  fieldJob: IDropDownItem<number>[] = [];
  country: IDropDownItem<number>[] = [];
  readonly DROPDOWN_JOB_STYLE = DROPDOWN_JOB_STYLE;
  mapFieldJob = {} as { [key: number]: string };
  mapCountryJob = {} as { [key: number]: string };
  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private helperService: HelperService,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({ ...DEFAULT_SEARCH });
    this.searchJob();
    this.getAllCountry();
    this.getAllField();
  }

  searchJob() {
    const request = {
      ...this.pagination,
      ...this.form.value
    }
    if (!request.fromSalary) {
      request.fromSalary = 0;
    }
    if (!request.toSalary) {
      request.toSalary = 0;
    }
    this.loading = true;
    this.apiService.searchJob(request)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ status, data, message }) => {
          if (status === ResponseStatus.success) {
            ({ data: this.jobs, pagination: { totalPage: this.totalPage } } = data);
          } else {
            this.helperService.showError('', message || 'Tìm kiếm công việc thất bại');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  onPageChange($event: PageChangedEvent) {
    this.pagination.pageNo = $event.page;
    this.searchJob();
  }

  get mapWorkingForm() {
    return MAP_JOB_STYLE_NAME;
  }

  getAllField() {
    this.apiService.getAllFieldJob().subscribe({
      next: ({ status, data }) => {
        if (status === ResponseStatus.success) {
          this.fieldJob = data.map(item => ({ value: item.id, label: item.fieldName }));
          this.mapFieldJob = data.reduce((acc, cur) => ({ ...acc, [cur.id]: cur.fieldName }), {});
        }
      }
    })
  }

  getAllCountry() {
    this.apiService.getAllCountry().subscribe({
      next: ({ status, data }) => {
        if (status === ResponseStatus.success) {
          this.country = data.map(item => ({ value: item.id, label: item.countryName }));
          this.mapCountryJob = data.reduce((acc, cur) => ({ ...acc, [cur.id]: cur.countryName }), {});
        }
      }
    })
  }

  report(item: JobDetail) {
    const modal = this.modalService.show(ReportJobComponent, {
      initialState: {
        job: item
      }
    });
    modal.content?.onClose.subscribe(value => {
      modal.hide();
      if (value) {
        console.log(value);
      }
    })
  }
  confirmAction() {
    const modal = this.modalService.show(ConfirmModalComponent, {
      backdrop: true,
      initialState: {
        message: `Bạn có chắc chấp thuận không? Khi thực hiện thì bài đăng công việc này sẽ bị ẩn đi`
      }
    });
    return modal.content?.onClose.pipe(
      tap(() => modal.hide())
    );
  }
  deleteJob(item: JobDetail) {
    this.confirmAction()?.subscribe(data => {
      if (data) {
        this.loading = true;
        this.apiService.deleteJob(item.jobId)
          .pipe(
            finalize(() => this.loading = true)
          )
          .subscribe({
            next: ({ status, data, message }) => {
              if (status === ResponseStatus.success) {
                this.helperService.showSuccess('', 'Ẩn tin tuyển dụng thành công');
                this.searchJob();
              } else {
                this.helperService.showError('', message || 'Ẩn tin tuyển dụng thất bại');
              }
            },
            error: error => this.helperService.callApiFailedHandler(error)
          })
      }
    });
  }
}
