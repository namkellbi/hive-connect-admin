import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagementRevenueComponent } from './management-revenue/management-revenue.component';
import { SharedLibraryModule } from '../../../shared/shared-library.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    ManagementRevenueComponent
  ],
  imports: [
    CommonModule,
    SharedLibraryModule,
    RouterModule.forChild([
      { path: '', component: ManagementRevenueComponent }
    ])
  ]
})
export class ManagementRevenueModule { }
