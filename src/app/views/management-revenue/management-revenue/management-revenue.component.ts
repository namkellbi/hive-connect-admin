import { Component, OnInit } from '@angular/core';
import { ResponseStatus } from '@interface/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { DEFAULT_PAGINATION } from '@util/defaultObject';
import moment from 'moment';
import { enGbLocale } from 'ngx-bootstrap/locale';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { finalize } from 'rxjs';
@Component({
  selector: 'app-management-revenue',
  templateUrl: './management-revenue.component.html',
  styleUrls: ['./management-revenue.component.scss']
})
export class ManagementRevenueComponent implements OnInit {
  pagination = { ...DEFAULT_PAGINATION, pageSize: 10 };
  filter = {
    startDate: moment().subtract(1, 'months').toDate(),
    endDate: moment().toDate()
  }
  loading = false;
  totals = 0;
  totalRevenue = 0;
  package: any[] = [];
  constructor(
    private apiService: ApiService,
    private helperService: HelperService,
    private localeService: BsLocaleService
  ) {
    enGbLocale.invalidDate = 'Ngày không hợp lệ';
    defineLocale('custom locale', enGbLocale);
    this.localeService.use('custom locale');
  }

  ngOnInit(): void {
    this.getRevenue();
  }

  search() {
    const { startDate, endDate } = this.filter;
    if (moment(startDate).isAfter(moment(endDate))) {
      this.helperService.showError('', 'Ngày bắt đầu phải trước ngày kết thúc');
      return;
    }
    this.pagination.pageNo = 1;
    this.getRevenue();
  }

  pageChange($event: PageChangedEvent) {
    this.pagination.pageNo = $event.page;
    this.getRevenue();
  }

  getRevenue() {
    this.loading = true;
    this.apiService.getTotalRevenue({
      ...this.pagination,
      startDate: moment(this.filter.startDate).format('yyyy-MM-DD'),
      endDate: moment(this.filter.endDate).format('yyyy-MM-DD'),
    })
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ status, data, message }) => {
          if (status === ResponseStatus.success) {
            this.package = data.data;
            this.totals = data.pagination.totalRecords;
            this.totalRevenue = data.totalRevenue;
          } else {
            this.helperService.showError('', message || 'lấy thống kê thất bại')
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }
  exportExcel() {
    const a = document.createElement('a');
    a.href = `${this.apiService.getApiV1('payment/export-revenue')}?startDate=${moment(this.filter.startDate).format('yyyy-MM-DD')}&endDate=${moment(this.filter.endDate).format('yyyy-MM-DD')}`;
    a.target = '_blank';
    a.click();
  }
}
