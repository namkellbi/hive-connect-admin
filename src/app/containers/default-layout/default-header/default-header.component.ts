import { Component, Input } from '@angular/core';

import { ClassToggleService, HeaderComponent } from '@coreui/angular';
import { AuthService } from '@services/auth.service';
import { StoreService } from '@services/store.service';

@Component({
  selector: 'app-default-header',
  templateUrl: './default-header.component.html',
})
export class DefaultHeaderComponent extends HeaderComponent {

  @Input() sidebarId: string = "sidebar";

  public newMessages = new Array(4)
  public newTasks = new Array(5)
  public newNotifications = new Array(5)
  info: any;
  constructor(
    private classToggler: ClassToggleService,
    private authService: AuthService,
    private storeService: StoreService
  ) {
    super();
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.storeService.userInfo.subscribe(data => this.info = data);
  }

  logout() {
    this.authService.logout();
  }
}
