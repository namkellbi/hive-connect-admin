import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { JobApplyState } from '@interface/job';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss']
})
export class ConfirmModalComponent implements OnInit {
  @Input() message = 'Bạn có chắc muốn thực hiện không?';
  @Output() onClose = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit(): void {
  }

}
