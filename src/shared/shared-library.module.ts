import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AvatarModule, BreadcrumbModule, FooterModule, DropdownModule, GridModule, HeaderModule, SidebarModule, NavModule, ButtonModule, FormModule, UtilitiesModule, ButtonGroupModule, SharedModule, TabsModule, ListGroupModule, ProgressModule, BadgeModule, CardModule, AccordionModule, PlaceholderModule, SpinnerModule, TooltipModule, CarouselModule, PaginationModule, PopoverModule, TableModule, ModalModule } from '@coreui/angular';
import { IconModule } from '@coreui/icons-angular';
import { DocsComponentsModule } from '@docs-components/docs-components.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ToastrModule } from 'ngx-toastr';
import { PaginationModule as BootstrapPaginationModule } from 'ngx-bootstrap/pagination';
import { ModalModule as BootstrapModalModule } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';


@NgModule({
  declarations: [],
  imports: [
    ToastrModule.forRoot({
      closeButton: true,
      // disableTimeOut: true,
      positionClass: 'toast-bottom-right'
      // toastComponent: ToastComponent
    }),
    FormsModule,
    ReactiveFormsModule,
    AvatarModule,
    BreadcrumbModule,
    FooterModule,
    DropdownModule,
    GridModule,
    HeaderModule,
    SidebarModule,
    IconModule,
    PerfectScrollbarModule,
    NavModule,
    ButtonModule,
    FormModule,
    UtilitiesModule,
    ButtonGroupModule,
    ReactiveFormsModule,
    SidebarModule,
    SharedModule,
    TabsModule,
    ListGroupModule,
    ProgressModule,
    BadgeModule,
    ListGroupModule,
    CardModule,
    ModalModule,

    AccordionModule,
    PlaceholderModule,
    SpinnerModule,
    TooltipModule,
    CarouselModule,
    PaginationModule,
    PopoverModule,
    TableModule,
    DocsComponentsModule,
    BsDatepickerModule.forRoot(),
    BootstrapPaginationModule.forRoot(),
    BootstrapModalModule.forRoot()
  ],
  exports: [
    ToastrModule,
    FormsModule,
    ReactiveFormsModule,
    AvatarModule,
    BreadcrumbModule,
    FooterModule,
    DropdownModule,
    GridModule,
    HeaderModule,
    SidebarModule,
    IconModule,
    PerfectScrollbarModule,
    NavModule,
    ButtonModule,
    FormModule,
    UtilitiesModule,
    ButtonGroupModule,
    ReactiveFormsModule,
    SidebarModule,
    SharedModule,
    TabsModule,
    ListGroupModule,
    ProgressModule,
    BadgeModule,
    ListGroupModule,
    CardModule,
    AccordionModule,
    PlaceholderModule,
    SpinnerModule,
    TooltipModule,
    CarouselModule,
    PaginationModule,
    PopoverModule,
    TableModule,
    DocsComponentsModule,
    BootstrapPaginationModule,
    BootstrapModalModule,
    ModalModule,
    BsDatepickerModule
  ]
})
export class SharedLibraryModule { }
