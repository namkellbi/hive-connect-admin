export const environment = {
  production: false,
  PREFIX: 'HC_ADMIN_',
  API_URL: 'https://hive.gogitek.online/',
  API_V1: `api/v1/`,
  USER_DOMAIN: 'https://hive-connect-social.web.app/'
};