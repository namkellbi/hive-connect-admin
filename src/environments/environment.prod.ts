export const environment = {
  production: true,
  PREFIX: 'HC_ADMIN_',
  API_URL: 'http://localhost:8081/',
  API_V1: `api/v1/`,
  USER_DOMAIN: 'https://hive-connect-social.web.app/'
};
