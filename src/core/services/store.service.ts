import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { STORAGE_KEY } from '../constant/setting';
import { IUser } from '../interfaces/user';
import { parseJSON } from '../utils/resourceHandle';
export type IUserObserver = IUser | null;
@Injectable({
  providedIn: 'root'
})
export class StoreService {

  userInfo = new BehaviorSubject<IUserObserver>(null);
  constructor() {
  }

  observerUser() {
    return this.userInfo.asObservable();
  }

  getAccessToken() {
    return this.getKey(STORAGE_KEY.ACCESS_TOKEN);
  }

  getRefreshToken() {
    return this.getKey(STORAGE_KEY.REFRESH_TOKEN);
  }

  setAccessToken(token: string) {
    this.setKey(STORAGE_KEY.ACCESS_TOKEN, token);
  }

  setRefreshToken(token: string) {
    this.setKey(STORAGE_KEY.REFRESH_TOKEN, token);
  }

  setUserInfo(info: IUser) {
    this.setKey(STORAGE_KEY.USER_INFO, JSON.stringify(info));
  }

  getUserInfo() {
    return parseJSON(this.getKey(STORAGE_KEY.USER_INFO), {} as IUser);
  }

  clearStore() {
    localStorage.clear();
  }

  saveAvatar(avatar: string) {
    const userInfo = this.userInfo.getValue();
    if (userInfo) {
      userInfo.avatar = avatar;
      this.userInfo.next(userInfo);
      this.setUserInfo(userInfo);
    }
  }

  setLatestUser(username: string) {
    localStorage.setItem(STORAGE_KEY.LATEST_USER, username);
  }

  getLatestUser() {
    return localStorage.getItem(STORAGE_KEY.LATEST_USER) || '';
  }

  getKey(key: string) {
    return localStorage.getItem(key) || '';
  }

  setKey(key: string, value: string) {
    localStorage.setItem(key, value);
  }
}
