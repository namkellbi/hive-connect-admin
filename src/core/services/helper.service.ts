import { Injectable } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { lastValueFrom, of } from 'rxjs';
import { DEFAULT_IMAGE_UPLOAD } from '../constant/form';
import { IRequestUploadImage } from '../interfaces/request';
import { ApiService } from './api.service';
import { StoreService } from './store.service';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor(
    private toastr: ToastrService,
    private storeService: StoreService,
    private apiService: ApiService
  ) { }

  showSuccess(title: string, content: string, option?: IndividualConfig) {
    return this.toastr.success(content, title, option);
  }

  showError(title: string, content: string, option?: IndividualConfig) {
    return this.toastr.error(content, title, option);
  }

  showWarning(title: string, content: string, option?: IndividualConfig) {
    return this.toastr.warning(content, title, option);
  }

  validateFormField(formGroup: FormGroup): void {
    // Object.keys(formGroup.controls).forEach(field => {
    //   const control = formGroup.get(field);
    //   if (control instanceof FormControl) {
    //     control.markAsTouched();
    //   } else if (control instanceof FormGroup) {
    //     this.validateFormField(control);
    //   }
    // });
    formGroup.markAsTouched();
  }

  uploadImage(file: File, settings: {
    isCompany: boolean,
    isAvatar: boolean,
    isBanner: boolean,
  } = {
      isCompany: false,
      isAvatar: true,
      isBanner: false,
    }) {
    const form = new FormData();
    form.append('file', file);
    form.append('imageRequest', this.handleImageInfo(settings));
    return lastValueFrom(this.apiService.uploadImage(form))
      .then(result => ({ url: result.data, type: file.type.split('/')[0] }));
  }

  handleImageInfo(settings: {
    isCompany: boolean,
    isAvatar: boolean,
    isBanner: boolean,
  } = {
      isCompany: false,
      isAvatar: true,
      isBanner: false,
    }) {
    const { id, roleId } = this.storeService.getUserInfo();
    const defaultRequest: IRequestUploadImage = { ...DEFAULT_IMAGE_UPLOAD, userId: id };
    if (settings.isCompany) {
      defaultRequest.type = 'CompanyImage';
    }
    if (settings.isAvatar) {
      defaultRequest.isAvatar = true;
    }
    if (settings.isBanner) {
      defaultRequest.isBanner = true;
    }
    return new Blob([JSON.stringify(defaultRequest)], { type: 'application/json' });
  }

  uploadFile(file: File) {
    const fileName = new Date().getTime().toString() + file.name;
    return new Promise((resolve, reject) => {
      resolve({ url: '', type: file.type.split('/')[0] });
    })
  }

  notSupported() {
    this.showWarning('', 'Tính năng đang được phát triển');
  }

  makeId(length: number) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() *
        charactersLength));
    }
    return result;
  }

  callApiFailedHandler(error: any, message?: string) {
    console.error('call failed', error);
    this.showError('', message || 'Xảy ra lỗi');
  }
}
