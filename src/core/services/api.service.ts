import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ICompany, ICompanyShort, ICountry, ILicense, Job, JobDetail, JobRecruiterView, ReportJob } from '@interface/job';
import { IBannerUserRequest, IPackageDetailResponse, PaymentRental } from '@interface/payment';
import { IChangeApplyRequestState, ICvViewerRequest, IFindJobRequest, ILoginRequest, IRegisterRequest, IRequestPagination } from '@interface/request';
import { IResponse, IResponsePagination, LoginResponse } from '@interface/response';
import { ICandidateCV, ICandidateProfile, ICertificate, ICVApply, IEducation, IFieldJob, IFieldMajor, ILanguage, IMajor, IOtherSkill, IRecruiterProfile, IUser, IWorkExperience } from '@interface/user';
import { filterObject } from '@util/resourceHandle';
import { environment } from '../../environments/environment.prod';
import { IBase } from '@interface/base';
import { map } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private httpClient: HttpClient,
  ) { }

  getApiV1(url: string) {
    return `${environment.API_URL}${environment.API_V1}${url}`;
  }

  indexPagination<T extends IBase>(response: IResponsePagination<T>) {
    if (response.data && response.data.data && response.data.data.length) {
      const { page, size } = response.data.pagination;
      response.data.data = response.data.data.map((item, index) => ({
        ...item,
        stt: ((+page - 1) * +size) + index + 1
      }));
    }
    return response;
  }

  login(params: ILoginRequest) {
    const url = this.getApiV1('login');
    return this.httpClient.post<LoginResponse>(url, params);
  }

  register(request: IRegisterRequest) {
    const url = this.getApiV1('register');
    return this.httpClient.post<IResponse<IUser> & { token: string }>(url, request);
  }

  getCandidateProfile(userId: number) {
    const url = this.getApiV1('candidate/find-by-userid');
    return this.httpClient.get<IResponse<ICandidateProfile>>(url, { params: { userId } });
  }
  getCandidateCV(candidateId: number) {
    const url = this.getApiV1('CV/get-cv');
    return this.httpClient.get<IResponse<ICandidateCV>>(url, { params: { candidateId } });
  }

  insertCvLanguage(params: ILanguage) {
    const url = this.getApiV1('CV/insert-language');
    return this.httpClient.post<IResponse<ILanguage>>(url, params);
  }

  insertCVWorkExp(params: IWorkExperience) {
    const url = this.getApiV1('CV/insert-work-exp');
    return this.httpClient.post<IResponse<IWorkExperience>>(url, params);
  }

  insertCVSkill(params: IOtherSkill) {
    const url = this.getApiV1('CV/insert-other-skill');
    return this.httpClient.post<IResponse<IOtherSkill>>(url, params);
  }

  insertCVEducation(params: IEducation) {
    const url = this.getApiV1('CV/insert-education');
    return this.httpClient.post<IResponse<IEducation>>(url, params);
  }

  insertCVCertificate(params: ICertificate) {
    return this.httpClient.post<IResponse<ICertificate>>(this.getApiV1('CV/insert-certificate'), params);
  }

  insertCVMajor(params: IMajor) {
    return this.httpClient.post<IResponse<IMajor>>(this.getApiV1('CV/insert-major-level'), params);
  }

  updateCVEducation(params: IEducation) {
    const url = this.getApiV1('CV/update-education');
    return this.httpClient.put<IResponse<IEducation>>(url, params);
  }

  updateCvSummary(params: { cvId: number; newSummary: string }) {
    return this.httpClient.post<IResponse<string>>(this.getApiV1('CV/update-cv-summary'), params);
  }

  updateOtherSkill(params: IOtherSkill) {
    return this.httpClient.put<IResponse<IOtherSkill>>(this.getApiV1('CV/update-other-skill'), params);
  }

  updateMajor(params: IMajor) {
    return this.httpClient.put<IResponse<IMajor>>(this.getApiV1('CV/update-major-level'), params);
  }

  updateLanguage(params: ILanguage) {
    return this.httpClient.put<IResponse<ILanguage>>(this.getApiV1('CV/update-language'), params);
  }

  updateDateWorkExp(params: IWorkExperience) {
    return this.httpClient.put<IResponse<IWorkExperience>>(this.getApiV1('CV/update-work-experience'), params);
  }

  updateCertificate(params: ICertificate) {
    return this.httpClient.put<IResponse<ICertificate>>(this.getApiV1('CV/update-certificate'), params);
  }

  deleteEducation(params: number) {
    return this.httpClient.put<IResponse<IEducation>>(this.getApiV1(''), params);
  }

  getAllFieldJob() {
    return this.httpClient.get<IResponse<IFieldJob[]>>(this.getApiV1('CV/get-all-field'));
  }

  getMajorOfField(fieldId: number) {
    return this.httpClient.get<IResponse<IFieldMajor[]>>(this.getApiV1('CV/get-major-by-field'), { params: { fieldId } });
  }

  getUrgentJob(params: IRequestPagination) {
    return this.httpClient.get<IResponsePagination<Job>>(this.getApiV1('job/urgent-job'), { params: { ...params } });
  }
  getRemoteJob() {
    return this.httpClient.get<IResponsePagination<Job>>(this.getApiV1('job/remote-job'));
  }
  getPopularJob(params: IRequestPagination) {
    return this.httpClient.get<IResponsePagination<Job>>(this.getApiV1('job/popular-job'), { params: { ...params } });
  }
  getParTimeJob() {
    return this.httpClient.get<IResponsePagination<Job>>(this.getApiV1('job/parttime-job'));
  }
  getNewJob(params: IRequestPagination) {
    return this.httpClient.get<IResponsePagination<Job>>(this.getApiV1('job/new-job'), { params: { ...params } });
  }
  getJobDetail(id: number, candidateId?: number) {
    return this.httpClient.get<IResponse<JobDetail>>(this.getApiV1(`job/job-detail/${id}`), { params: { candidateId: candidateId || -1 } });
  }
  getJobByField(id: number) {
    return this.httpClient.get<IResponsePagination<Job>>(this.getApiV1(`job/job-by-field`), { params: { id } });
  }
  getFullTimeJob() {
    return this.httpClient.get<IResponsePagination<Job>>(this.getApiV1(`job/fulltime-job`));
  }
  searchJob(request: IFindJobRequest) {
    return this.httpClient.get<IResponsePagination<JobDetail>>(this.getApiV1(`job/find-job`), { params: { ...request } }).pipe(
      map(response => this.indexPagination(response))
    );
  }
  getSuggestJob(id: number) {
    return this.httpClient.get<IResponse<Job[]>>(this.getApiV1(`job/suggest-job/${id}`));
  }
  getAllCountry() {
    return this.httpClient.get<IResponse<ICountry[]>>(this.getApiV1('common/get-vietnam-country'));
  }

  getRecruiterJobCreate(request: IRequestPagination & { recruiterId: number }) {
    return this.httpClient.get<IResponsePagination<JobRecruiterView>>(this.getApiV1('job/get-jobs-of-recruiter'), { params: { ...request } })
  }

  getRecruiterProfile(userId: number) {
    return this.httpClient.get<IResponse<IRecruiterProfile>>(this.getApiV1(`recruiter/recruiter-profile/${userId}`));
  }

  getCVAppliedJob(jobId: number) {
    return this.httpClient.get<IResponse<any>>(this.getApiV1('job/list-cv-applied-a-job'), { params: { jobId } });
  }

  getCompany() {
    return this.httpClient.get<IResponse<ICompany[]>>(this.getApiV1('company/get-list-company'));
  }

  createJob(request: any) {
    return this.httpClient.post<IResponse<Job>>(this.getApiV1('job/create-job'), request);
  }

  applyJob(request: any) {
    return this.httpClient.post<IResponse>(this.getApiV1('job/apply-job'), request);
  }

  deleteWorkExp(id: number) {
    return this.httpClient.delete<IResponse<IWorkExperience>>(this.getApiV1('CV/delete-work-experience'), { params: { id } });
  }
  deleteOtherSkill(id: number) {
    return this.httpClient.delete<IResponse<IOtherSkill>>(this.getApiV1('CV/delete-other-skill'), { params: { id } });
  }
  deleteMajor(id: number) {
    return this.httpClient.delete<IResponse<IMajor>>(this.getApiV1('CV/delete-major-level'), { params: { id } });
  }
  deleteLanguage(id: number) {
    return this.httpClient.delete<IResponse<ILanguage>>(this.getApiV1('/api/v1/CV/delete-language'), { params: { id } });
  }
  deleteCertificate(id: number) {
    return this.httpClient.delete<IResponse<ICertificate>>(this.getApiV1('/api/v1/CV/delete-certificate'), { params: { id } });
  }

  getCvAppliedJob(jobId: number, request: IRequestPagination) {
    return this.httpClient.get<IResponsePagination<ICVApply>>(this.getApiV1(`job/list-cv-applied-a-job/${jobId}`), { params: { ...request } });
  }

  approveJob(params: IChangeApplyRequestState) {
    return this.httpClient.put<IResponse<any>>(this.getApiV1('job/approve-job'), params);
  }

  createCompany(params: any) {
    return this.httpClient.post<IResponse<ICompany>>(this.getApiV1('company/create-company'), params);
  }

  resendEmailVerify(username: string) {
    return this.httpClient.post<IResponse<unknown>>(this.getApiV1(`resend-email/${username}`), {});
  }

  confirmEmailToken(token: string) {
    return this.httpClient.post<IResponse<IUser> & { token: string }>(this.getApiV1('confirm-account'), {}, {
      params: { token }
    })
  }

  uploadImage(params: FormData) {
    return this.httpClient.post<IResponse<string>>(this.getApiV1('files/upload-image'), params);
  }

  createCV(id: number) {
    return this.httpClient.get<IResponse<ICandidateCV>>(this.getApiV1('CV/create-cv'), { params: { candidateId: id } });
  }

  updateIsNeededJob(candidateId: number) {
    return this.httpClient.put<IResponse<unknown>>(this.getApiV1(`candidate/update-is-need-job`), {}, { params: { candidateId } });
  }

  updateBaseProfile(params: any) {
    return this.httpClient.put<IResponse<ICandidateProfile>>(this.getApiV1('candidate/update-candidate-base-information'), params);
  }

  getTopRecruitmentCompany() {
    return this.httpClient.get<IResponse<ICompanyShort[]>>(this.getApiV1('company/get-top-12-recruitment-companies'));
  }

  getCvViewer(params: ICvViewerRequest) {
    return this.httpClient.get<IResponse<unknown>>(this.getApiV1('candidate/get-cv-viewer'), { params: { ...params } });
  }

  getListCompany() {
    return this.httpClient.get<IResponse<ICompany[]>>(this.getApiV1('company/get-list-company'));
  }

  updateRecruiterProfile(params: unknown) {
    return this.httpClient.put<IResponse<IRecruiterProfile>>(this.getApiV1('recruiter/update-recruiter-profile'), params);
  }

  searchUser(params: any) {
    return this.httpClient.get<IResponsePagination<any>>(this.getApiV1('admin/search-users'), { params }).pipe(
      map(response => this.indexPagination(response))
    )
  }

  searchReportUser(params: any) {
    return this.httpClient.get<IResponse<any>>(this.getApiV1('admin/search-reported-users'), { params });
  }

  getAccountInfo() {
    return this.httpClient.get<IResponse<any>>(this.getApiV1('admin/count-users'));
  }
  activeDeActive(userId: number) {
    return this.httpClient.put<IResponse<any>>(this.getApiV1(`admin/active-deactive-user?userId=${userId}`), {});
  }
  lockUnlock(userId: number) {
    return this.httpClient.put<IResponse<any>>(this.getApiV1(`admin/lock-unlock-user?userId=${userId}`), {});
  }
  listRentalPackage() {
    return this.httpClient.get<IResponse<any>>(this.getApiV1(`package/list-rental-package`));
  }
  listPackage(params: { name: string; rentalPackageId: PaymentRental }) {
    return this.httpClient.get<IResponsePagination<any>>(this.getApiV1('package/list-package'), { params: { ...filterObject(params) } }).pipe(
      map(response => this.indexPagination(response))
    );
  }
  createRentalPackage(params: any) {
    return this.httpClient.post<IResponse<any>>(this.getApiV1('package/create-rental-package'), params);
  }
  createPackage(params: any) {
    return this.httpClient.post<IResponse<any>>(this.getApiV1('package/create-new-package'), params);
  }
  updatePackage(params: any) {
    return this.httpClient.put<IResponse<any>>(this.getApiV1('package/update-new-package'), params);
  }
  getDetailPackage(packageId: number, groupPackageId: number) {
    return this.httpClient.get<IResponse<IPackageDetailResponse>>(this.getApiV1(`package/package`), { params: { packageId, groupPackageId } });
  }
  getListReport(pagination: IRequestPagination) {
    return this.httpClient.get<IResponsePagination<any>>(this.getApiV1('admin/get-all-reported'), { params: { ...pagination } });
  }
  getReportJobs(params: any) {
    return this.httpClient.get<IResponsePagination<ReportJob>>(this.getApiV1('admin/get-reported-job'), { params: params }).pipe(
      map(response => this.indexPagination(response))
    );
  }
  getLicense(params: any) {
    return this.httpClient.get<IResponse<ILicense[]>>(this.getApiV1('admin/get-licenses-approval'), { params })
  }
  changeLicenseStatus(params: any) {
    return this.httpClient.put<IResponse<any>>(this.getApiV1('admin/approve-license'), params);
  }
  getListBanner(params: any) {
    return this.httpClient.get<IResponsePagination<any>>(this.getApiV1('banner/get-all-banner-package'), { params: { ...filterObject(params) } }).pipe(
      map(response => this.indexPagination(response))
    );
  }
  createPackageBanner(params: any) {
    return this.httpClient.post<IResponse<any>>(this.getApiV1('banner/config-banner'), params);
  }
  updateBannerPackage(params: any) {
    return this.httpClient.put<IResponse<any>>(this.getApiV1('banner/update-banner-package'), params);
  }
  getDetailBannerPackage(bannerPackageId: number) {
    return this.httpClient.get<IResponse<any>>(this.getApiV1(`banner/detail-banner-package`), { params: { bannerPackageId } });
  }
  getBannerRequest(params: any) {
    return this.httpClient.get<IResponsePagination<IBannerUserRequest>>(this.getApiV1('banner/get-banner-for-approval'), { params: { ...params } }).pipe(
      map(response => this.indexPagination(response))
    );
  }
  updateBannerRequest(params: any) {
    return this.httpClient.put<IResponse>(this.getApiV1('banner/approve-banner'), params);
  }
  deleteJob(id: number) {
    return this.httpClient.delete<IResponse>(this.getApiV1('job/delete-job'), { params: { id } });
  }
  updateStatusReport(params: any) {
    return this.httpClient.put<IResponse>(this.getApiV1('admin/approve-reported'), params, { params: { ...params } });
  }
  getTotalRevenue(params: any) {
    return this.httpClient.get<IResponse<any> & { pagination: any }>(this.getApiV1('payment/total-revenue'), { params }).pipe(
      map(response => this.indexPagination(response) as any)
    );
  }
  getUserInfo(id: number) {
    return this.httpClient.get<IResponse<IUser>>(this.getApiV1(`user/user-infor/${id}`));
  }
  getStatistic() {
    return this.httpClient.get<IResponse<any>>(this.getApiV1('admin/count-recruitment-statistic'));
  }
}
