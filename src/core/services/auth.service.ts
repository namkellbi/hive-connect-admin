import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { IRegisterRequest } from '../interfaces/request';
import { LoginResponse } from '../interfaces/response';
import { ICandidateProfile, IRecruiterProfile } from '../interfaces/user';
import { ApiService } from './api.service';
import { StoreService } from './store.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private apiService: ApiService,
    private storeService: StoreService,
    private router: Router,
  ) { }

  async loginSuccess(data: LoginResponse) {
    const { data: { candidate, recruiter, user }, token } = data;
    this.storeService.setAccessToken(token);
    this.storeService.setRefreshToken(token);
    this.storeService.setLatestUser(user.email);
    this.storeService.setUserInfo(user);
    this.storeService.userInfo.next(user);
  }

  // getUserInfo(username: string) {
  //   return lastValueFrom(this.apiService.getBasicProfileUser(username));
  // }

  logout() {
    this.storeService.clearStore();
    this.signOut();
    this.router.navigate(['/login']).then(() => window.location.reload());
  }

  signOut(): void {
  }

  register(request: IRegisterRequest) {
    return this.apiService.register(request);
  }
}
