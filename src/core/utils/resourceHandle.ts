import { Observable, lastValueFrom } from "rxjs";

export const validURL = (str: string) => {
  var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
  if (RegExp.test(str)) {
    return true;
  } else {
    return false;
  }
}

export const isBase64Image = (str: string) => {
  if (str === '' || str === undefined || str === null) { return false; }

  if (str.indexOf('data:image/') == 0) {
    return true;
  } else {
    return false;
  }
}

export const validatePromise = <T>(api: Observable<T>): Promise<T | null> => {
  try {
    return lastValueFrom(api);
  } catch (err) {
    return new Promise(resolve => resolve(null));
  }
}

export const copyObjectValue = (from: any, to: any, callback: ((data: any) => boolean) = (data) => true) => {
  Object.keys(from).forEach(key => {
    if (from[key] && callback(from[key])) {
      to[key] = from[key];
    }
  })
}

export const parseJSON = <T>(str: string, defaultValue: T): T => {
  try {
    return JSON.parse(str) as T;
  } catch (error) {
    return defaultValue;
  }
}

export const updateArray = <T>(data: T, arr: T[], ...keys: (keyof T)[]): T[] => {
  const index = arr.findIndex(item => keys.every(key => item[key] === data[key]));
  if (index > -1) {
    arr.splice(index, 1, data);
  } else {
    arr.unshift(data);
  }
  return arr.slice();
}

export const filterObject = (params: any) => {
  const obj = Object.keys(params)
    .filter(key => typeof params[key] === 'boolean' ? true : !!params[key])
    .reduce((acc, cur) => ({ ...acc, [cur]: params[cur] }), {});
  return obj;
}