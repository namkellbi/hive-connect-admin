import { AbstractControl, ValidationErrors } from "@angular/forms";

export const checkPassword = (group: AbstractControl): ValidationErrors | null => {
  const pass = group.get('password')?.value;
  const confirmPass = group.get('matchPassword')?.value;
  if (!confirmPass) {
    return { required: true };
  }
  if (pass !== confirmPass) {
    // group.get('password')?.setErrors({ 'notMatch': true });
    group.get('matchPassword')?.setErrors({ 'notMatch': true });
    return { notSame: true };
  }
  group.get('matchPassword')?.setErrors(null);
  return null
}

export const checkRangeSalary = (group: AbstractControl): ValidationErrors | null => {
  const from = group.get('fromSalary')?.value;
  const to = group.get('toSalary')?.value;
  if (!to || !from) {
    return { required: true }
  }
  if (to < from) {
    group.get('fromSalary')?.setErrors({ 'largerThanRange': true });
    group.get('toSalary')?.setErrors({ 'smallerThanRange': true });
    return { outOfRange: true };
  }
  group.get('fromSalary')?.setErrors(null);
  group.get('toSalary')?.setErrors(null);
  return null;
}

// export const getImageUpload = (event: Event): FileUpload | null => {
//   const ele = event.target as HTMLInputElement;
//   if (ele.files) {
//     const result: FileUpload = { url: '', file: ele.files[0] };
//     const reader = new FileReader();
//     reader.readAsDataURL(ele.files[0]);
//     reader.onload = (_event) => {
//       result.url = reader.result;
//     }
//     return result;
//   }
//   return null
// }

export const noWhiteSpace = (control: AbstractControl): ValidationErrors | null => {
  const value = control.value as string;
  const regex = /([\p{L}\p{N}])+/gui;
  if (!value.match(regex)) {
    return { required: true };
  }
  return null;
}
