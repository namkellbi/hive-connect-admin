import { Observable, of } from "rxjs";
import { IRequestPagination, IFindJobRequest } from "../interfaces/request";
import { IResponse, IResponsePagination, ResponseStatus } from "../interfaces/response";

export const DEFAULT_PAGINATION: IRequestPagination = {
  pageNo: 1,
  pageSize: 10
}

export const DEFAULT_SEARCH: Omit<IFindJobRequest, 'pageNo' | 'pageSize'> = {
  countryId: 0,
  fieldId: 0,
  fromSalary: 0,
  toSalary: 0,
  jobName: '',
  rank: '',
  workForm: '',
  workPlace: ''
}

export const defaultRequest = <T>(): Observable<IResponse<T>> => of({ status: ResponseStatus.error, data: {} as T, message: '' });
export const defaultPaginationRequest = <T>(): Observable<IResponsePagination<T>> => of({
  status: ResponseStatus.error, data: {
    ...{ status: ResponseStatus.error, data: [] as T[], message: '' },
    pagination: {
      page: 0,
      size: 0,
      totalPage: 0,
      totalRecords: 0,
    }
  }, message: ''
});