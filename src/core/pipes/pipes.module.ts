import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageConvertPipe } from './image-convert.pipe';
import { AvatarConverterPipe } from './avatar-converter.pipe';



@NgModule({
  declarations: [
    ImageConvertPipe,
    AvatarConverterPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ImageConvertPipe,
    AvatarConverterPipe
  ]
})
export class PipesModule { }
