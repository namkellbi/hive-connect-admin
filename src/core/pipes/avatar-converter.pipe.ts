import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'avatarConverter'
})
export class AvatarConverterPipe implements PipeTransform {

  transform(value: unknown, isCover: boolean = false): unknown {
    // if (value instanceof ArrayBuffer || value instanceof File) {
    //   return value;
    // }
    // if (isBase64Image(value as string)) {
    //   return value;
    // }
    // if (!value) {
    //   return isCover ? DEFAULT_COVER_URL : DEFAULT_AVATAR_URL;
    // }
    // return `${environment.IMAGE_PATH}${value}`;
    return value;
  }

}
