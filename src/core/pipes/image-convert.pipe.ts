import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'imageConvert'
})
export class ImageConvertPipe implements PipeTransform {

  transform(url: any, isCover?: boolean): unknown {
    // if (url instanceof ArrayBuffer || url instanceof File) {
    //   return url;
    // }
    // if (validURL(url)) {
    //   return url;
    // } else if (isBase64Image(url)) {
    //   return url;
    // } else if (!url) {
    //   return isCover ? DEFAULT_COVER_URL : DEFAULT_AVATAR_URL;
    // } else {
    //   return `${environment.API_URL}${url.substring(1)}`;
    // }
    return url;
  }

}
