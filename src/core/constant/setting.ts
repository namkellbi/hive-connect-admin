import { environment } from "../../environments/environment.prod";

const PREFIX = environment.PREFIX;

const STORAGE_KEY = {
  ACCESS_TOKEN: PREFIX + 'jwt_access_token',
  REFRESH_TOKEN: PREFIX + 'refresh_token',
  USER_INFO: PREFIX + 'user_info',
  EXPIRED: PREFIX + 'jwt_expired',
  CHAT_TOKEN: PREFIX + 'chat_access_token',
  CHAT_ID: PREFIX + 'chat_id',
  LATEST_USER: PREFIX + 'latest_user'
}

export {
  STORAGE_KEY
}