import { JobApplyState, JobLevel, WorkStyle } from "../interfaces/job"
import { IRequestUploadImage } from "../interfaces/request";
import { Gender } from "../interfaces/user";
import { IDropDownItem } from "../interfaces/form"
import { PaymentRental } from "@interface/payment";

const FORM_ERRORS: { [key: string]: { [key: string]: string } } = {
  email: {
    required: 'Email không được để trống',
    email: 'Email không đúng định dạng'
  },
  password: {
    required: 'Mật khẩu không được để trống',
    notMatch: 'Mật khẩu không trùng'
  },
  username: {
    required: 'Tên hiển thị không được để trống',

  },
  base: {
    required: 'Không được bỏ trống',
    largerThanRange: 'Số lớn giới hạn',
    smallerThanRange: 'Số nhỏ hơn giới hạn'
  }
}


const MAP_JOB_STYLE_NAME = {
  [WorkStyle.fulltime]: 'Toàn thời gian',
  [WorkStyle.partTime]: 'Bán thời gian',
  [WorkStyle.remote]: 'Làm từ xa'
}
const MAP_GENDER_VALUE = {
  ['null']: 'Không xác định',
  ['true']: 'Nam',
  ['false']: 'Nữ'
}
const MAP_JOB_LEVEL_VALUE = {
  [JobLevel.one]: "Cơ bản",
  [JobLevel.two]: 'Chưa có kinh nghiệm',
  [JobLevel.three]: 'Thành thạo',
  [JobLevel.four]: 'Chuyên gia'
}

const MAP_JOB_STATE_VALUE = {
  [JobApplyState.approve]: 'Duyệt',
  [JobApplyState.pending]: 'Đang xử lý',
  [JobApplyState.reject]: 'Từ chối'
}

const MAP_RENTAL_PACKAGE: { [key in PaymentRental]: string } = {
  [PaymentRental.advertisement]: 'Dịch vụ chính',
  [PaymentRental.candidate]: 'Gói mở hồ sơ ứng viên',
  [PaymentRental.banner]: 'Banner quảng cáo',
}

const DROPDOWN_LEVEL: IDropDownItem<JobLevel>[] = Object.entries(JobLevel).map(([key, value]) => ({ value: value, label: MAP_JOB_LEVEL_VALUE[value] }));
const DROPDOWN_GENDER: IDropDownItem<Gender>[] = Object.entries(Gender).map(([key, value]) => ({ value: value, label: MAP_GENDER_VALUE[value] }));
const DROPDOWN_JOB_STYLE: IDropDownItem<WorkStyle>[] = Object.entries(WorkStyle).map(([key, value]) => ({ value: value, label: MAP_JOB_STYLE_NAME[value] }));
const DROPDOWN_JOB_STATUS: IDropDownItem<JobApplyState>[] = Object.entries(JobApplyState).map(([key, value]) => ({ value: value, label: MAP_JOB_STATE_VALUE[value] }));
const DROPDOWN_RENTAL_PACKAGE: IDropDownItem<PaymentRental>[] = Object.entries(PaymentRental).map(([key, value]) => ({ value: value, label: MAP_RENTAL_PACKAGE[value] }));
const DEFAULT_IMAGE_UPLOAD: IRequestUploadImage = {
  candidatePostId: 0,
  companyId: 0,
  eventId: 0,
  isAvatar: false,
  isBanner: false,
  recruiterPostId: 0,
  type: 'Avatar',
  userId: 0
}
export {
  FORM_ERRORS,
  DROPDOWN_LEVEL,
  MAP_JOB_STYLE_NAME,
  MAP_GENDER_VALUE,
  DROPDOWN_JOB_STYLE,
  DROPDOWN_GENDER,
  DEFAULT_IMAGE_UPLOAD,
  MAP_JOB_LEVEL_VALUE,
  MAP_JOB_STATE_VALUE,
  DROPDOWN_JOB_STATUS,
  DROPDOWN_RENTAL_PACKAGE,
  MAP_RENTAL_PACKAGE
}