import { AfterViewInit, Directive, ElementRef, Input, OnChanges, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[hcButtonLoading]'
})
export class ButtonLoadingDirective implements OnChanges, AfterViewInit {
  @Input() loading = false;
  btnContent: string = '';
  constructor(private el: ElementRef) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.btnContent != '') {
      this.el.nativeElement.disabled = this.loading;
      if (this.loading) {
        this.el.nativeElement.innerHTML = '';
        this.el.nativeElement.insertAdjacentHTML('beforeend', '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
      } else {
        this.el.nativeElement.innerHTML = this.btnContent;
      }
    } else {
      this.btnContent = this.el.nativeElement.outerText || this.el.nativeElement.innerHTML;
    }
  }

  ngAfterViewInit(): void {
    this.btnContent = this.el.nativeElement.outerText || this.el.nativeElement.innerHTML;
  }
}
