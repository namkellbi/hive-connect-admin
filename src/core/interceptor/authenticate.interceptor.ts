import {
  HttpErrorResponse, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { StoreService } from '../services/store.service';
@Injectable()
export class AuthenticateInterceptor implements HttpInterceptor {
  constructor(
    private storeService: StoreService,
    private authService: AuthService
  ) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const token = this.storeService.getAccessToken();
    const headers: any = {};

    if (token) {
      headers['Authorization'] = 'Bearer ' + token;
    }

    request = request.clone({ headers: new HttpHeaders(headers) });

    return next.handle(request).pipe(catchError(err => {
      if (err instanceof HttpErrorResponse && !request.url.includes('signup') && err.status === 401) {
        this.authService.logout();
      }
      return throwError(() => err);
    }));
  }
}
