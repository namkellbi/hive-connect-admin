import { JobApplyState } from "./job";
import { ROLE } from "./user";

export interface ILoginRequest {
  username: string;
  password: string;
}

export interface IRegisterRequest {
  username: string;
  password: string;
  confirmPassword: string;
  email: string;
  roleId: ROLE;
}

export interface IRequestPagination {
  pageNo: number;
  pageSize: number;
}

export interface IFindJobRequest extends IRequestPagination {
  fieldId: number;
  countryId: number;
  jobName: string;
  fromSalary: number;
  toSalary: number;
  rank: string;
  workForm: string;
  workPlace: string;
}

export interface ICvViewerRequest extends IRequestPagination {
  cvId: number;
  candidateId: number;
}

export interface IChangeApplyRequestState {
  jobId: number;
  candidateId: number;
  approvalStatus: JobApplyState;
  createdAt: string;
  updatedAt: string;
}

export interface IRequestUploadImage {
  userId: number;
  companyId: number;
  eventId: number;
  candidatePostId: number;
  recruiterPostId: number;
  isAvatar: boolean;
  isBanner: boolean;
  type: 'Avatar' | 'CompanyImage'; //type Avatar || CompanyImage
}