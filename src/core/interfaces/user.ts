import { JobApplyState, JobLevel } from "./job";
import { BaseResponse } from "./response";

export interface IUser {
  id: number;
  username: string;
  password: string;
  email: string;
  roleId: ROLE;
  isDeleted: number;
  lastLoginTime: string;
  avatar: string;
  verifiedEmail: boolean;
  verifiedPhone: boolean;
  createdAt: string;
  updatedAt: string;
  phone: string;
  fullName: string;
  locked: boolean;
}

export enum ROLE {
  admin = 1,
  recruiter,
  candidate,
  counselors
}

export enum Gender {
  unknown = 'null',
  male = 'true',
  female = 'false'
}

export interface ICandidateProfile extends BaseResponse {
  id: number;
  userId: number;
  gender: boolean;
  birthDate: string;
  country: string;
  fullName: string;
  address: string;
  socialLink: string;
  avatarUrl: string;
  isNeedJob: boolean;
  experienceLevel: number;
  introduction: string;
}

export interface ICertificate {
  certificateName: string;
  certificateUrl: string;
  status: number;
  cvId: number;
  id: number;
}

export class Certificate implements ICertificate {
  certificateName!: string;
  certificateUrl!: string;
  status!: number;
  cvId!: number;
  id!: number;
}

export interface IEducation {
  id: number;
  cvId: number;
  school: string;
  major: string;
  startDate: string;
  endDate: string;
  isStudying: boolean;
  description: string;
}

export class Education implements IEducation {
  id!: number;
  cvId!: number;
  school!: string;
  major!: string;
  startDate!: string;
  endDate!: string;
  isStudying!: boolean;
  description!: string;

}
export interface ILanguage {
  id: number;
  language: string;
  level: JobLevel;
  cvId: number;
}

export class Language implements ILanguage {
  id!: number;
  language!: string;
  level!: JobLevel;
  cvId!: number;

}

export interface IMajor {
  id: number;
  fieldId: number;
  majorId: number;
  cvId: number;
  level: JobLevel;
  status: boolean;
  fieldName: string;
  majorName: string;
}

export class Major implements IMajor {
  id!: number;
  fieldId!: number;
  majorId!: number;
  cvId!: number;
  level!: JobLevel;
  status!: boolean;
  majorName!: string;
  fieldName!: string;
}
export interface IOtherSkill {
  id: number;
  skillName: string;
  cvId: number;
  level: JobLevel;
}

export class OtherSkill implements IOtherSkill {
  id!: number;
  skillName!: string;
  cvId!: number;
  level!: JobLevel;

}

export interface IWorkExperience {
  id: number;
  cvId: number;
  companyName: string;
  position: string;
  startDate: string;
  endDate: string;
  description: string;
}

export class WorkExperiences implements IWorkExperience {
  id!: number;
  cvId!: number;
  companyName!: string;
  position!: string;
  startDate!: string;
  endDate!: string;
  description!: string;

}
export interface ICandidateCV {
  id: number;
  candidateId: number;
  isDeleted: number;
  summary: string;
  createdAt: string;
  updatedAt: string;
  certificates: ICertificate[];
  educations: IEducation[];
  languages: ILanguage[];
  majorLevels: IMajor[];
  otherSkills: IOtherSkill[];
  workExperiences: IWorkExperience[];
}

export interface IFieldJob {
  id: number;
  fieldName: string;
}

export interface IFieldMajor {
  id: number;
  fieldId: number;
  majorName: string
}

export interface IRecruiterProfile {
  id: number;
  companyId: number;
  companyName: string;
  fullName: string;
  verifyAccount: boolean;
  verifyPhoneNumber: boolean;
  gender: string;
  position: string;
  linkedinAccount: string;
  businessLicense: string;
  userId: number;
  phoneNumber: string;
  companyAddress: string;
  recruiterId: number;
}

export interface ICVApply {
  jobId: number;
  candidateId: number;
  candidateName: string;
  avatar: string;
  experienceDesc: string[];
  educations: string[];
  careerGoal: string;
  address: string;
  cvUrl: string;
  approvalStatus: JobApplyState;
}