export interface IUserPost {
  id: number;
  username: string;
  displayName: string;
  avatar: string;
}

export interface ICommentPost {
  content: string;
  react: number;
  createdAt: string;
  user: IUserPost;
}

export interface IAttachmentPost {
  type: AttachmentPost,
  url: string;
}

export enum AttachmentPost {
  image = "image",
  video = "video"
}

export interface IPost {
  id: number;
  user: IUserPost,
  content: string,
  attachment: IAttachmentPost[],
  createdAt: string;
  react: number;
  comments: ICommentPost[]
}