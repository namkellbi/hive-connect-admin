import { IBase } from "./base";
import { JobApplyState } from "./job";

export enum PaymentRental {
  candidate = '1',
  advertisement = '2',
  banner = '3'
}

export interface IBannerUserRequest extends IBase {
  applyEndDate: string;
  applyStartDate: string;
  approvalDate: string;
  approvalStatus: JobApplyState;
  bannerActiveId: number;
  buyDate: string;
  companyId: number;
  companyName: string;
  displayPosition: string;
  packageName: string;
  recruiterName: string;
  updatedAt: string;
}
export interface IPackageDetailResponse {
  bannerPackage: any;
  normalPackage: any;
}
export enum BannerPosition {
  spotlight = 'spotlight',
  homepageBannerA = 'homepageBannerA',
  homepageBannerB = 'homepageBannerB',
  homepageBannerC = 'homepageBannerC',
  jobBannerA = 'jobBannerA',
  jobBannerB = 'jobBannerB',
  jobBannerC = 'jobBannerC'
}