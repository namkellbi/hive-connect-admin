import { IBase } from "./base";
import { Gender, ICandidateCV } from "./user";

export enum WorkFrom {
  remote = 'REMOTE',
  fulltime = 'FULLTIME',
  parttime = 'PARTTIME'
}

export enum JobLevel {
  one = "Fundamental Awareness",
  two = "Limited Experience",
  three = "Intermediate",
  four = "Professional"
}

export enum WorkStyle {
  remote = "REMOTE",
  fulltime = "FULLTIME",
  partTime = 'PARTTIME'
}

export enum JobApplyState {
  approve = 'Approved',
  reject = 'Reject',
  pending = 'Pending'
}
export interface Job extends IBase {
  jobId: number;
  id: number;
  companyId: number;
  recruiterId: number;
  listHashtag: string[];
  companyName: string;
  jobName: string;
  jobDescription: string;
  jobRequirement: string;
  benefit: string;
  fromSalary: number;
  toSalary: number;
  numberRecruits: number;
  rank: string;
  workForm: WorkStyle;
  gender: Gender;
  startDate: string;
  endDate: string;
  workPlace: string;
  createdAt: string;
  updatedAt: string;
  isPopularJob: boolean;
  isNewJob: boolean;
  isUrgentJob: boolean;
  isDeleted: number;
  weekday: string;
}

export interface ICompany {
  address: string;
  avatar: string;
  description: string;
  email: string;
  fieldWork: string;
  id: number;
  isDeleted: number;
  mapUrl: string;
  name: string;
  numberEmployees: string;
  phone: string;
  taxCode: string;
  website: string;
}
export interface ICompanyShort {
  companyId: number;
  applyCvNumber: number;
  companyAvatar: string;
  companyName: string;
}
export interface JobDetail extends Job {
  experience: string;
  applied: boolean;
  approvalStatus: JobApplyState;
  company: ICompany;
}

export interface ICountry {
  id: number;
  countryName: string;
}

export interface JobRecruiterView {
  jobId: number;
  jobName: string;
  companyName: string;
  workPlace: string;
  fromSalary: number;
  toSalary: number;
  viewCount: number;
  applyCount: number;
  endDate: string;
}

export interface JobReport {
  jobName: string;
  companyName: string;
  fullName: string;
  phone: string;
  createdAt: string;
  updatedAt: string;
}

export interface ILicense extends IBase {
  requestUserId: number;
  requestUserName: string;
  businessLicenseUrl: string;
  businessLicenseApprovalStatus: JobApplyState,
  additionalLicenseUrl: string;
  additionalLicenseApprovalStatus: JobApplyState;
  createdAt: string;
  updatedAt: string;
}

export interface ReportJob extends IBase {
  approvalReportedStatus: JobApplyState;
  companyName: string;
  createdAt: string;
  fullName: string;
  jobId: number;
  jobName: string;
  personReportId: number;
  phone: string;
  reportId: number;
  reportedUserId: number;
  updatedAt: string;
  userAddress: string;
  userEmail: string;
}