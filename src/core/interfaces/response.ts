import { ICandidateProfile, IRecruiterProfile, IUser } from "./user";

export enum ResponseStatus {
  success = 'Success',
  error = 'Error'
}

export interface IResponse<T = unknown> {
  data: T;
  status: ResponseStatus;
  message: string;
}

export interface LoginResponse extends IResponse<{
  user: IUser;
  candidate: ICandidateProfile;
  recruiter: IRecruiterProfile;
  admin: {
    fullName: string;
  }
}> {
  token: string;
}

export interface BaseResponse<key = 'id'> {
  key: number;
}

export type IResponsePagination<T> = IResponse<IResponse<T[]> & {
  pagination: {
    page: number;
    size: number;
    totalPage: number;
    totalRecords: number;
  }
}> 
